import { Component, OnInit } from '@angular/core';

import {FormGroup, FormBuilder, Validator, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TarjetaCredito } from 'src/app/models/tarjetaCredito';
import { TarjetaService } from 'src/app/services/tarjeta.service';



@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.scss']
})
export class TarjetaCreditoComponent implements OnInit {
  form: FormGroup;

  constructor(private formbuilder: FormBuilder,
              private tarjetaService: TarjetaService,
              private toastr: ToastrService) { 
    this.form= this.formbuilder.group({
      Id:0,
      Titular: ['', [Validators.required]],
      NumeroTarjeta: ['', [Validators.required, Validators.maxLength(16), Validators.minLength(16)]],
      FechaExpiracion: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(5)]],
      cvv: ['', [Validators.required, Validators.maxLength(3), Validators.minLength(3)]]



    })
  }

  ngOnInit(): void {
  }

guardarTarjeta(){

  const tarjeta: TarjetaCredito = {
    Titular: this.form.get('Titular')?.value,
    NumeroTarjeta: this.form.get('NumeroTarjeta')?.value,
    FechaExpiracion: this.form.get('FechaExpiracion')?.value,
    cvv: this.form.get('cvv')?.value
  }

  this.tarjetaService.guardarTarjeta(tarjeta).subscribe(data=> {
    //console.log('Guardado con exito...');
    this.toastr.success('Guardado con exito...','gracias por registrarce!.');
    this.form.reset();
  });
}

}

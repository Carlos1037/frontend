export class TarjetaCredito {
    
    Id?: number;
    Titular?: string;
    NumeroTarjeta?: string;
    FechaExpiracion?: string;
    cvv?: string;
}